function Stack(){
		this.els = [];
	}
	
	Stack.prototype = {
	
		push: function( el ){
			this.els.push( el );
		},
		
		peek: function(){
			if( this.els.length > 0 ){
				return this.els[ this.els.length - 1 ];
			}else{
				return null;
			}
		},
		
		pop: function(){
			var el = this.els[ this.els.length - 1 ];
			this.els = this.els.slice( 0, this.els.length - 1 );
			return el;
		},
		
		size: function(){
			return this.els.length;
		}
	
	};

function HashTable(o)
{
  this.length = 0;
  this.entries = {};
  this.init(o);
}

HashTable.prototype = {
  init: function(o)
  {
    for(var prop in o)
    if(o.hasOwnProperty(prop))
    {
      this.entries[prop] = o[prop];
      this.length++;
    }
  },

  put: function(k,v)
  {
    var oldval = undefined;
    if(this.isThere(k))
      oldval = this.entries[k];
    else
      this.length++;
    this.entries[k] = v;
  },

  get: function(k)
  {
    return this.isThere(k) ? this.entries[k] : undefined;
  },

  isThere: function(k)
  {
    return this.entries.hasOwnProperty(k);
  },

  remove: function(k)
  {
    if(this.isThere(k))
    {
      var old = this.entries[k];
      this.length--;
      delete this.entries[k];
      return old;
    }
    else
      return undefined;
  },

  keys: function()
  {
    var ks = [];
    for(var k in this.entries)
      if(this.isThere(k))
          ks.push(k);
    return ks;
  },

  vals: function()
  {
    var vs = [];
    for (var k in this.entries)
      if(this.isThere(k))
        vs.push(this.entries[k]);
    return vs;
  },

  forEach: function(fn)
  {
    var vs = [];
    for (var k in this.entries)
      if(this.isThere(k))
        fn(k, this.entries[k]);
  },

  clear: function()
  {
    this.entries=[];
    this.length=0;
  }
};
