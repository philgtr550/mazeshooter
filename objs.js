"use strict";

function Projectile(x, y, angle, id)
{
  this.id = id;
  this.x = x;
  this.y = y;
  this.moveRate = 4;
  this.angle = angle;
  this.dmg = 5;
}

Projectile.prototype = 
{
  move: function()
  {
    var nextX = this.x+this.moveRate*Math.cos(this.angle*(Math.PI/180)),
        nextY = this.y+this.moveRate*Math.sin(this.angle*(Math.PI/180));

    var intersections = level.testCollision(this.x,this.y,nextX,nextY);
    if(!intersections.n && !intersections.s && !intersections.w && !intersections.e)
    {
      this.x = nextX;
      this.y = nextY;
    }
    else
      projectiles.remove(this.id);

    return {x: this.x, y: this.x};
  },

  hit: function(victim)
  {
    if(victim.ship.isShot(this.x,this.y))
    {
      victim.health -= this.dmg;
      console.log('%s was hit', victim.name);
      if(victim.health <= 0)
      {
        console.log('%s is dead', victim.name);
        victim.dead = true;
      }
      projectiles.remove(this.id);
    }
  }

};

function KBTracker()
{
  this.up=false;
  this.down=false;
  this.left=false;
  this.right=false;

  this.shoot=false;
}

function Maze(w,h,cellMapSize, cellWorldSize)
{
  this.cells=[];                      // row major order
  this.cellMapSize=cellMapSize;       // map cell size in pixels
  this.cellWorldSize=cellWorldSize;   // cell size for movement (world coord)
  this.startI = 0;
  this.startJ = 0;
  this.w=w;
  this.h=h;
  this.init();
  this.generateMaze();
}

Maze.prototype= {

  worldCoordToMapCoord: function(xw,yw)
  {
    var cellI = yw / this.cellWorldSize,
        cellJ = xw / this.cellWorldSize;

    return {x:Math.floor(cellJ*this.cellMapSize), y:Math.floor(cellI*this.cellMapSize)};
  },

  getCell: function(i,j)
  {
    return this.cells[i*this.h + j];
  },

  worldCoordToCellCoord: function(xw,yw)
  {
    var cellI= Math.floor(yw / this.cellWorldSize),
        cellJ= Math.floor(xw / this.cellWorldSize);

    return {xc: xw - (cellJ * this.cellWorldSize), 
            yc: yw - (cellI * this.cellWorldSize),
            i: cellI,
            j: cellJ};
  },

  generateMaze: function()  // 'stupid' DFS
  {
    var st = new Stack();
    var cells = this.w*this.h;
    var curI = ~~(Math.random()*this.h);
    var curJ = ~~(Math.random()*this.w);
    this.startI = curI;
    this.startJ = curJ;
    var visitedCells = 1;

    while(visitedCells < cells || st.length > 0)
    {
      var unvisitedNeighbors = this.unvisitedNeighbors(curI,curJ);
      var unvisitedCells = unvisitedNeighbors.length;
      if(unvisitedCells>=1)
      {
        var curNeighbor = unvisitedNeighbors[~~(Math.random()*unvisitedNeighbors.length)];
          
        this.removeWall(curI,curJ,curNeighbor);
        st.push(this.cells[curI*this.h + curJ]);
        this.cells[curI*this.h + curJ].visited = true;
        curNeighbor.visited = true;

        curI=curNeighbor.i;
        curJ=curNeighbor.j;

        visitedCells+=1;
      }
      else
      {
        var cell = st.pop();
        curI=cell.i;
        curJ=cell.j;
      }
    }
  },

  init: function()
  {
    for(var i=0; i<this.h; i++){
      for(var j=0; j<this.w; j++)
      {
        var c = new Cell(i,j,this.cellMapSize, this.cellWorldSize);
        this.cells.push(c);
      }
    }
  },

  removeWall: function(i,j,neighbor)
  {
    // southern neighbor
    if(i>0 && this.cells[(i-1)*this.h+j]===neighbor)
    {
      this.cells[i*this.h+j].s=false;
      neighbor.n=false;
    }

    // northern neighbor
    if(i<this.h-1 && this.cells[(i+1)*this.h+j]===neighbor)
    {
      this.cells[i*this.h+j].n=false;
      neighbor.s=false;
    }

    // western neighbor
    if(j>0 && this.cells[i*this.h+(j-1)]===neighbor)
    {
      this.cells[i*this.h+j].w=false;
      neighbor.e=false;
    }

    // eastern neighbor
    if(j<this.w-1 && this.cells[i*this.h+(j+1)]===neighbor)
    {
      neighbor.w=false;
      this.cells[i*this.h+j].e=false;
    }

    return;
  },

  unvisitedNeighbors: function(i,j)
  {
    var neighbors = [];

    // south
    if(i>0 && !this.cells[(i-1)*this.h+j].visited)
      neighbors.push(this.cells[(i-1)*this.h+j]);

    // north
    if(i<this.h-1 && !this.cells[(i+1)*this.h+j].visited)
      neighbors.push(this.cells[(i+1)*this.h+j]);

    // west
    if(j>0 && !this.cells[i*this.h+(j-1)].visited)
      neighbors.push(this.cells[(i)*this.h+(j-1)]);

    // east
    if(j<this.w-1 && !this.cells[i*this.h+(j+1)].visited)
      neighbors.push(this.cells[(i)*this.h+(j+1)]);

    return neighbors;
  },

  // show map
  paintMazeMap: function(pts,scale)
  {
    for(var i = 0; i < this.h; i++)
      for(var j = 0; j < this.w; j++)
        this.cells[(i*this.h)+j].paintMap(pts,scale);
  },

  testCollision: function(x1,y1, x2, y2)
  {
    var cell1I = Math.floor(y1 / this.cellWorldSize),
        cell1J = Math.floor(x1 / this.cellWorldSize),
        cell2I = Math.floor(y2 / this.cellWorldSize),
        cell2J = Math.floor(x2 / this.cellWorldSize);
    
    var startcell = this.getCell(cell1I, cell1J);

    var intersections = startcell.findWallIntersection(x1,y1,x2,y2);

    return intersections;
  },

  testShipCollision: function(ship1, ship2)
  {
    
  }
};

function Cell(i,j,cellMapSize, cellWorldSize)
{
  this.cellMapSize=cellMapSize;
  this.cellWorldSize=cellWorldSize;
  this.i=i;
  this.j=j;
  this.visited=false;

  // indicates walls
  this.n=true;
  this.s=true;
  this.w=true;
  this.e=true;
}

Cell.prototype = 
{
  findWallIntersection: function(x1,y1,x2,y2)
  {

    // (x,y) transformed from world to single-cell coordinates
    var x1cell = x1 - (this.j * this.cellWorldSize),        
        y1cell = y1 - (this.i * this.cellWorldSize),
        x2cell = x2 - (this.j * this.cellWorldSize),
        y2cell = y2 - (this.i * this.cellWorldSize);

    var cellI1 = Math.floor(y1 / this.cellWorldSize),
        cellJ1 = Math.floor(x1 / this.cellWorldSize),
        cellI2 = Math.floor(y2 / this.cellWorldSize),
        cellJ2 = Math.floor(x2 / this.cellWorldSize);

    var collisions = {s: (cellI1 > cellI2 || y2cell <= 0) && this.s,
                      n: (cellI1 < cellI2 || y2cell >= this.cellWorldSize) && this.n,
                      w: (cellJ1 > cellJ2 || x2cell <= 0) && this.w,
                      e: (cellJ1 < cellJ2 || x2cell >= this.cellWorldSize) && this.e};

    return collisions;
  },

  // DRY...
  paintMap: function(pts)
  {
    // compute wall points
    // The points are aligned to the canvas's coord. system
    // i.e. (0,   0) is the top left corner, 
    //      (cMS, cMS) is the bottom right corner
    var x, y;
    x = this.j*this.cellMapSize;
    y = this.i*this.cellMapSize;
    if(this.e)
    {
      pts.push(x+this.cellMapSize);
      pts.push(y);
      pts.push(x+this.cellMapSize);
      pts.push(y+this.cellMapSize);
    }
    if(this.w)
    {
      pts.push(x);
      pts.push(y);
      pts.push(x);
      pts.push(y+this.cellMapSize);
    }
    if(this.s)
    {
      pts.push(x);
      pts.push(y);
      pts.push(x+this.cellMapSize);
      pts.push(y);
    }
    if(this.n)
    {
      pts.push(x);
      pts.push(y+this.cellMapSize);
      pts.push(x+this.cellMapSize);
      pts.push(y+this.cellMapSize);
    }
  },

  computeWallWorldPoints: function(wallPts)
  {
    // Wall points are aligned to the world's coordinate system
    // (0,0) is the lower left corner
    var x, y;
    x = this.j*this.cellWorldSize;
    y = this.i*this.cellWorldSize;
    if(this.e)
    {
      pts.push(x+this.cellWorldSize);
      pts.push(y);
      pts.push(x+this.cellWorldSize);
      pts.push(y+this.cellWorldSize);
    }
    if(this.w)
    {
      pts.push(x);
      pts.push(y);
      pts.push(x);
      pts.push(y+this.cellWorldSize);
    }
    if(this.s)
    {
      pts.push(x);
      pts.push(y);
      pts.push(x+this.cellWorldSize);
      pts.push(y);
    }
    if(this.n)
    {
      pts.push(x);
      pts.push(y+this.cellWorldSize);
      pts.push(x+this.cellWorldSize);
      pts.push(y+this.cellWorldSize);
    }
    return wallPts;
  }
};

function Weapon(name, dmg, speed, firerate, weptype)
{
  this.name=name;
  this.damage=dmg;
  this.speed=speed;
  this.firerate=firerate;
  this.weptype=weptype;
}

function Character(name, health, startCellI, startCellJ, weapon)
{
  this.name=name;
  this.health=health;
  this.cellI=startCellI;
  this.cellJ=startCellJ;
  
  // the character's center of mass
  this.worldX = startCellJ*level.cellWorldSize + level.cellWorldSize/2; 
  this.worldY = startCellI*level.cellWorldSize + level.cellWorldSize/2;

  this.ship = new Ship(this.worldX, this.worldY);

  this.weapon=weapon;
  this.moveRate=2;
  this.curRotAngle=0;
  this.dTheta=7.5;
  this.dead = false;
}

Character.prototype = {

  move: function(reverse)
  {
    var nextX = this.worldX+this.moveRate*Math.cos(this.curRotAngle*(Math.PI/180)),
        nextY = this.worldY+this.moveRate*Math.sin(this.curRotAngle*(Math.PI/180));

    if(reverse)
    {
      nextX=this.worldX-this.moveRate*Math.cos(this.curRotAngle*(Math.PI/180));
      nextY=this.worldY-this.moveRate*Math.sin(this.curRotAngle*(Math.PI/180));
    }

    var ship2 = new Ship(nextX,nextY);

    // collisions are based off of the ship's center of mass/character world position
    var intersections = level.testCollision(this.worldX,this.worldY,nextX,nextY);
    if(!intersections.n && !intersections.s && !intersections.w && !intersections.e)
    {
      this.worldX = nextX;
      this.worldY = nextY;

      // update ship
      this.ship = ship2;
    }

    return {x: this.worldX, y: this.worldY};
  },
  
  rotate: function(left)
  {
    this.curRotAngle+=(this.dTheta*left);
    if(this.curRotAngle>360)
      this.curRotAngle-=360;
    if(this.curRotAngle<-360)
      this.curRotAngle+=360;
  },

  shoot: function()
  {
    projectiles.put(proj_id,new Projectile(this.worldX, this.worldY, this.curRotAngle, proj_id));
    proj_id++;
  }
};

// wrapper for creating the drawable ship triangle thing
function Ship(x,y)
{
  this.cm =        [x,   y]
  this.tip =       [x,   y+10];
  this.backLeft =  [x-5, y-5];
  this.backRight = [x+5, y-5];
}

Ship.prototype = 
{
  // check if projectile location is in the triangle ship
  // done via barycentric coordinates
  isShot: function(x,y)
  {
    var t = this.triangleArea(this.tip,this.backLeft,this.backRight);

    var a = this.triangleArea([x,y],this.tip,this.backLeft) / t ;
    var b  = this.triangleArea([x,y],this.backLeft,this.backRight) / t ;
    var c = this.triangleArea([x,y],this.backRight,this.tip) / t ;

  if ((a>=0) && (b>=0) && (c>=0) && (Math.abs(a+b+c-1)<0.01)) return true;
  else return false;
  },

  triangleArea: function(a,b,c)
  {
    var area = ((b[1] - c[1]) * (a[0] - c[0]) + (c[0] - b[0]) * (a[1] - c[1]));
    area = Math.abs(0.5*area);
    return area;
  }
};
