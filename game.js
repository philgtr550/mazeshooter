"use strict";

// Globs ... yes I know there's a lot
var canvasMap,
    canvasGame;
var glGame,
    glMap;
var program;
var fps=45;
var kbt;    // keyboard event tracker
var t2;     
var proj_id = 0;  //projectile ID

// shader variables for the game scene and map
var a_Position_Game;
var u_ModelMatrix_Game;
var u_FragColor_Game;

var u_ModelMatrix_Map;
var u_FragColor_Map;
var a_Position_Map;

// vars for game logic
var player;
var bosschar;
var enemies;
var projectiles;
var level;
var pause;
var positions;  //character to ship table


function createWorldMap()
{
  var wallPts = [];
  level.paintMazeMap(wallPts,canvasMap.width / level.cellSize);
  return wallPts;
}

function initMaze(w,h)
{
  level = new Maze(w,h,canvasMap.width / w ,100);
}

function initCharacters(numOpps)
{
  player = new Character("Snake",100,level.startJ,level.startI,null);
  bosschar   = new Character("McPixel", 1000, 0,0,null);
  positions = new HashTable({player: player.ship, boss: bosschar.ship});
  for(var idx = 0; idx < numOpps; idx++)
  {
    var i = ~~(Math.random()*level.w),
        j = ~~(Math.random()*level.h);

    while(i == player.cellI && j == player.cellJ)
    {
      i = ~~(Math.random()*level.w);
      j = ~~(Math.random()*level.h);
    }

    enemies.push(new Character("Roger Wilco Clone #" + (idx+1), 50, j,i, null));
    positions.put(enemies[idx].name, enemies[idx].ship);
  }
}

function main()
{
  canvasGame = document.getElementById('gameScene');
  canvasMap = document.getElementById('gameMap');
  glGame = getWebGLContext(canvasGame);
  glMap  = getWebGLContext(canvasMap);
  kbt = new KBTracker();
  enemies = [];
  projectiles = new HashTable();
  pause = false;
  if(!glGame || !glMap)
  {
    console.log('Can\'t get one of the WebGL rendering contexts...');
    return;
  }

  glGame.viewportWidth = glMap.viewportWidth = canvasGame.width;
  glGame.viewportHeight = glMap.viewportHeight = canvasGame.height;  
  console.log('Viewport dimensions: %d x %d', glGame.viewportWidth, glGame.viewportHeight);

  if(!initShaders(glGame,VSHADER_SOURCE_SCENE, FSHADER_SOURCE_SCENE) || !initShaders(glMap, VSHADER_SOURCE_MAP, FSHADER_SOURCE_MAP))
  {
    console.log('Can\'t start shaders...');
    return;
  }

  u_ModelMatrix_Game = glGame.getUniformLocation(glGame.program,'u_ModelMatrix');
  u_FragColor_Game = glGame.getUniformLocation(glGame.program, 'u_FragColor');
  u_ModelMatrix_Map = glMap.getUniformLocation(glMap.program,'u_ModelMatrix');
  u_FragColor_Map = glMap.getUniformLocation(glMap.program, 'u_FragColor');

  if(!u_ModelMatrix_Map || !u_FragColor_Map || !u_ModelMatrix_Game || !u_FragColor_Game)
  {
    console.log('Couldn\'t get one of the shader variables :(');
    return;
  }

  // KB listeners
  window.addEventListener("keydown",handleKBOnPress,true);
  window.addEventListener("keyup",handleKBOnLift,true);

  initMaze(4,4);
  initCharacters(4);
  initSceneGL();

  var n = initMapGL();
  if(n<0)
    console.log('Map initialization failed');

  tick();
}

function animate()
{
  var t1 = Date.now(),
      elapsed = t1 - t2;
  if(elapsed < 1000/fps) return; 

  t2 = t1;

  // check for keyboard triggers and edit character accordingly
  checkForPlayerMovement();
  recomputeEnemyPos();
  // check character health values
}

function recomputeEnemyPos()
{
  var projs = projectiles.vals();
  for(var j = 0; j < projs.length; j++)
  {
    projs[j].move();
    for(var k = 0; k < enemies.length; k++)
    {
      projs[j].hit(enemies[k]);
    }
  }

  for(var i = 0; i < enemies.length; i++)
  {
    if(enemies[i].dead)
      enemies.splice(i,1);
    else
    {
      var chosenValue = Math.random() < 0.5 ? 1 : -1;
      enemies[i].rotate(chosenValue);
      enemies[i].move(0);
      positions.put(enemies[i].name, enemies[i].ship);
    }
  }
}

function checkForPlayerMovement()
{
  if(kbt.up)
    player.move(false);
  if(kbt.down)
    player.move(true);
  if(kbt.left)
    player.rotate(1);
  if(kbt.right)
    player.rotate(-1);
  if(kbt.shoot)
    player.shoot();
  positions.put('player', player.ship) ;
}

function initMapGL()
{
  var buffer = glMap.createBuffer();
  if(!buffer)
  {
    console.log('Can\'t create the buffer for maze map');
    return 0;
  }

  glMap.bindBuffer(glMap.ARRAY_BUFFER, buffer);
  var a_Position = glMap.getAttribLocation(glMap.program, 'a_Position');
  if(a_Position < 0)
  {
    console.log('Can\'t get a_Position');
    return 0;
  }

  glMap.vertexAttribPointer(a_Position, 2, glMap.FLOAT, false, 0,0);
  glMap.enableVertexAttribArray(a_Position);

  return 0;
}

function initSceneGL()
{
  var buffer = glGame.createBuffer();
  if(!buffer)
  {
    console.log('Can\'t create the buffer for maze map');
    return 0;
  }

  glGame.bindBuffer(glGame.ARRAY_BUFFER, buffer);
  var a_Position = glGame.getAttribLocation(glGame.program, 'a_Position');
  if(a_Position < 0)
  {
    console.log('Can\'t get a_Position');
    return 0;
  }

  glGame.vertexAttribPointer(a_Position, 2, glGame.FLOAT, false, 0,0);
  glGame.enableVertexAttribArray(a_Position);

  return 0;
}

function drawMap()
{
  glMap.clearColor(0,0,0,1);
  glMap.clear(glMap.COLOR_BUFFER_BIT); 

  glMap.viewport(0,0,glMap.viewportWidth, glMap.viewportHeight);

  var t = 2,
      triangle;

  mvMatrix.setIdentity();
  var left=-5.0, right=5.0, bottom=-5.0, top=5.0, near=0, far=10.0;
  pMatrix.setIdentity();
  pMatrix.ortho(left,right,bottom,top,near,far);
  mvMatrix.multiply(pMatrix);
  
  mvMatrix.lookAt(0,0,10, 0,0,0, 0,1,1);

  // Resizes the world to fit the 400x400 canvas
  mvMatrix.translate(-5, -5, 0);          
  mvMatrix.scale(1/40, 1/40, 0);
  mvPushMatrix();

  //  Draw each enemy's location on the map
  for(var i = 0; i < enemies.length; i++)
  { 
    mvPushMatrix();

    mvMatrix.translate(enemies[i].worldX, enemies[i].worldY, 0);
    mvMatrix.rotate(enemies[i].curRotAngle - 90,0,0,1);
    mvMatrix.translate(-enemies[i].worldX, -enemies[i].worldY, 0);

    glMap.uniformMatrix4fv(u_ModelMatrix_Map, false, mvMatrix.elements);    

    triangle = [enemies[i].ship.tip[0],       enemies[i].ship.tip[1],
                enemies[i].ship.backLeft[0],  enemies[i].ship.backLeft[1],
                enemies[i].ship.backRight[0], enemies[i].ship.backRight[1],
                enemies[i].worldX,            enemies[i].worldY];

    glMap.bufferData(glMap.ARRAY_BUFFER, new Float32Array(triangle), glMap.DYNAMIC_DRAW);
    glMap.uniform4f(u_FragColor_Map,1,0,0,1);
    glMap.drawArrays(glMap.TRIANGLES, 0,3);
    glMap.uniform4f(u_FragColor_Map, 0,0,1,1);
    glMap.drawArrays(glMap.POINTS, 3 , 1);

    mvPopMatrix();
  }
  
  // transform player object
  mvMatrix.translate(player.worldX, player.worldY, 0);
  mvMatrix.rotate(player.curRotAngle - 90,0,0,1);
  mvMatrix.translate(-player.worldX, -player.worldY, 0);

  // apply MVP for player position
  glMap.uniformMatrix4fv(u_ModelMatrix_Map, false, mvMatrix.elements);

  // draw player object
  triangle = [player.ship.tip[0],       player.ship.tip[1],
              player.ship.backLeft[0],  player.ship.backLeft[1],
              player.ship.backRight[0], player.ship.backRight[1],
              player.worldX,            player.worldY];
  glMap.bufferData(glMap.ARRAY_BUFFER, new Float32Array(triangle), glMap.DYNAMIC_DRAW);
  glMap.uniform4f(u_FragColor_Map,1,1,1,1);
  glMap.drawArrays(glMap.TRIANGLES, 0,3);
  glMap.uniform4f(u_FragColor_Map, 0,0,0,1);
  glMap.drawArrays(glMap.POINTS, 3 , 1);
  
  mvPopMatrix();  // remove player-specific transformation

  // reapply MVP for the map wall drawing
  glMap.uniformMatrix4fv(u_ModelMatrix_Map, false, mvMatrix.elements);

  // draw map
  var wallPts = createWorldMap();
  glMap.bufferData(glMap.ARRAY_BUFFER, new Float32Array(wallPts), glMap.STATIC_DRAW);
  glMap.uniform4f(u_FragColor_Map, 1,1,1,1);
  glMap.drawArrays(glMap.LINES,0,wallPts.length / 2);
}


function drawScene()
{
  glGame.clearColor(0,0,0,1);
  glGame.clear(glGame.COLOR_BUFFER_BIT);
  glGame.viewport(0,0,glGame.viewportWidth, glGame.viewportHeight);

  mvMatrix.setIdentity();
  var left=-5.0, right=5.0, bottom=-5.0, top=5.0, near=0, far=10.0;
  pMatrix.setIdentity();
  pMatrix.ortho(left,right,bottom,top,near,far);
  mvMatrix.multiply(pMatrix);
  
  mvMatrix.lookAt(0,0,10, 0,0,0, 0,1,1);

  for(var i = 0; i < enemies.length; i++)
  {
    mvPushMatrix();

    var everts = [enemies[i].ship.tip[0],       enemies[i].ship.tip[1],
                  enemies[i].ship.backLeft[0],  enemies[i].ship.backLeft[1],
                  enemies[i].ship.backRight[0], enemies[i].ship.backRight[1],
                  enemies[i].worldX,    enemies[i].worldY];
    mvMatrix.scale(1/20, 1/20, 0); 
    mvMatrix.translate(-player.worldX, -player.worldY, 0);

    mvMatrix.translate(enemies[i].worldX, enemies[i].worldY, 0);
    mvMatrix.rotate(enemies[i].curRotAngle - 90,0,0,1);
    mvMatrix.translate(-enemies[i].worldX, -enemies[i].worldY, 0);

    glGame.uniformMatrix4fv(u_ModelMatrix_Game, false, mvMatrix.elements);
    glGame.bufferData(glGame.ARRAY_BUFFER, new Float32Array(everts), glGame.DYNAMIC_DRAW);
    glGame.uniform4f(u_FragColor_Game,1*(enemies[i].health / 50.0),0,0,1);
    glGame.drawArrays(glGame.TRIANGLES, 0,3);
    glGame.uniform4f(u_FragColor_Game, 0,0,1,1);
    glGame.drawArrays(glGame.POINTS, 3 , 1);
    
    mvPopMatrix();
  }

  mvPushMatrix();
  
  mvMatrix.scale(1/20, 1/20, 0); 
  mvMatrix.translate(-player.worldX, -player.worldY, 0);

  mvMatrix.translate(player.worldX, player.worldY, 0);
  mvMatrix.rotate(player.curRotAngle - 90,0,0,1);
  mvMatrix.translate(-player.worldX, -player.worldY, 0);

  glGame.uniformMatrix4fv(u_ModelMatrix_Game, false, mvMatrix.elements);

  var triangle = [player.ship.tip[0],       player.ship.tip[1],
                  player.ship.backLeft[0],  player.ship.backLeft[1],
                  player.ship.backRight[0], player.ship.backRight[1],
                  player.worldX,            player.worldY];
  glGame.bufferData(glGame.ARRAY_BUFFER, new Float32Array(triangle), glGame.DYNAMIC_DRAW);
  glGame.uniform4f(u_FragColor_Game,1,1,1,1);
  glGame.drawArrays(glGame.TRIANGLES, 0,3);
  glGame.uniform4f(u_FragColor_Game, 0,0,1,1);
  glGame.drawArrays(glGame.POINTS, 0 , 4);
  mvPopMatrix();

  for(var idx = 0; idx < proj_id; idx++)
  {
    if(projectiles.isThere(idx))
    {
      mvPushMatrix();
  
      mvMatrix.scale(1/20, 1/20, 0); 
      mvMatrix.translate(-player.worldX, -player.worldY, 0);
  
      mvMatrix.translate(projectiles.get(idx).x, projectiles.get(idx).y, 0);
      mvMatrix.rotate(projectiles.get(idx).angle,0,0,1);
      mvMatrix.translate(-projectiles.get(idx).x, -projectiles.get(idx).y, 0);
  
  
      glGame.uniformMatrix4fv(u_ModelMatrix_Game, false, mvMatrix.elements);
    
      glGame.bufferData(glGame.ARRAY_BUFFER, new Float32Array([projectiles.get(idx).x, projectiles.get(idx).y]), glGame.DYNAMIC_DRAW);
      glGame.uniform4f(u_FragColor_Game, 1,1,1,1);
      glGame.drawArrays(glGame.POINTS, 0 , 1);
      mvPopMatrix();
    }
  }

  mvMatrix.scale(1/20, 1/20, 0); 
  mvMatrix.translate(-player.worldX, -player.worldY, 0);
  // draw map
  var wallPts = createWorldMap();
  glGame.uniformMatrix4fv(u_ModelMatrix_Game, false, mvMatrix.elements);
  glGame.bufferData(glMap.ARRAY_BUFFER, new Float32Array(wallPts), glGame.STATIC_DRAW);
  glGame.uniform4f(u_FragColor_Game, 1,1,1,1);
  glGame.drawArrays(glGame.LINES,0,wallPts.length / 2);
}

function tick()
{
  var cont = true;
  if(!pause)
    cont = animate();
  drawMap();
  drawScene();
  if(enemies.length == 0)
    return;
  requestAnimationFrame(tick, canvasMap);
}

function handleKBOnPress(ev)
{
  switch(ev.keyCode)
  {
    case 37:    //left, 'a' pressed
    case 65:
      kbt.left=true;
      break;

    case 39:
    case 68:    //right, 'd' pressed
      kbt.right=true;
      break;

    case 38:
    case 87:    //up, 'w' pressed
      kbt.up=true;
      break;

    case 40:    //down, 's' pressed
    case 83:
      kbt.down=true;
      break;

    case 32:    // shoot something
      kbt.shoot=true;
      break;
      
    case 80:
      pause = !pause;
      console.log(pause ? 'Game paused' : 'Game resumed');
      break;
  }
}

function handleKBOnLift(ev)
{
  switch(ev.keyCode)
  {
    case 37:    //left, 'a' pressed
    case 65:
      kbt.left=false;
      break;

    case 39:
    case 68:    //right, 'd' pressed
      kbt.right=false;
      break;

    case 38:
    case 87:    //up, 'w' pressed
      kbt.up=false;
      break;

    case 40:    //down, 's' pressed
    case 83:
      kbt.down=false;
      break;

    case 32:    // shoot something
      kbt.shoot=false;
      break;
  }
}

function freeContexts()
{
  glMap = null;
  glGame = null;
}
